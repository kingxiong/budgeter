﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KingBudget.Startup))]
namespace KingBudget
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
