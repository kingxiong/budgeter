﻿using System.Collections.Generic;

namespace KingBudget.Models
{
    public class Budget
    {
        public Budget()
        {
            this.Items = new HashSet<Item>();
            this.Transactions = new HashSet<Transaction>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int HouseHoldId { get; set; }
        public int CategoryId { get; set; }
        public double Amount { get; set; }
        //public string Item { get; set; }
        public string Description { get; set; }
        

        public virtual HouseHold HouseHold { get; set; }
        public virtual Category Category { get; set; }

        public virtual ICollection<Item> Items { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }

    }
}