﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace KingBudget.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public ApplicationUser()
        {
            this.HouseHolds = new HashSet<KingBudget.Models.HouseHold>();
            this.BankAccounts = new HashSet<KingBudget.Models.BankAccount>();
            this.Transactions = new HashSet<KingBudget.Models.Transaction>();
            this.Invites = new HashSet<KingBudget.Models.Invite>();
            //this.Items = new HashSet<KingBudget.Models.Item>();

        }
        public virtual ICollection<HouseHold> HouseHolds { get; set; }
        public virtual ICollection<BankAccount> BankAccounts { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public virtual ICollection<Invite> Invites { get; set; }
        //public virtual ICollection<Item> Items { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<KingBudget.Models.HouseHold> HouseHolds { get; set; }

        public System.Data.Entity.DbSet<KingBudget.Models.BankAccount> BankAccounts { get; set; }

        public System.Data.Entity.DbSet<KingBudget.Models.Invite> Invites { get; set; }

        public System.Data.Entity.DbSet<KingBudget.Models.Item> Items { get; set; }

        public System.Data.Entity.DbSet<KingBudget.Models.Budget> Budgets { get; set; }

        public System.Data.Entity.DbSet<KingBudget.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<KingBudget.Models.Transaction> Transactions { get; set; }
        public System.Data.Entity.DbSet<KingBudget.Models.Type> Types { get; set; }

    }
}