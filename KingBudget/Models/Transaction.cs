﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KingBudget.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int BankAccountId { get; set; }
        public string Description { get; set; }
        public string Payee { get; set; }


        [DisplayFormat(DataFormatString = "{0:C}")] 
        public double Amount { get; set; }

        public int TypeId { get; set; }
        public int CategoryId { get; set; }
        public string EnteredById { get; set; }
        public bool IsDeleted { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTimeOffset Created { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTimeOffset? Updated { get; set; }


        public virtual BankAccount BankAccount { get; set; }
        public virtual KingBudget.Models.Type Type { get; set; }
        public virtual ApplicationUser EnteredBy { get; set; }
        public virtual Category Category { get; set; }

    }
}