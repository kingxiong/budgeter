﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KingBudget.Models
{
    public class HouseHold
    {
        public HouseHold()
        {
            this.Users = new HashSet<ApplicationUser>();
            this.Budgets = new HashSet<Budget>();
            this.BankAccounts = new HashSet<BankAccount>();
            this.Items = new HashSet<Item>();
            //this.Categories = new HashSet<Category>();
        }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string CreatorId { get; set; }
        public double Balance { get; set; }
        public bool IsDeleted { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset Created { get; set; }
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset? Updated { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ICollection<BankAccount> BankAccounts { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual ICollection<Budget> Budgets { get; set; }
        public virtual ICollection<Invite> Invites { get; set; }
    }
}