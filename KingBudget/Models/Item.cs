﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KingBudget.Models
{
    public class Item
    {
        public int Id { get; set; }
        [Required]
        //public string Name { get; set; }
        public int CategoryId { get; set; }
        public int HouseHoldId { get; set; }
        //public int BudgetId { get; set; }
        public double Amount { get; set; }
        public bool IsDeleted { get; set; }
        public double TotalAmount { get; set; }
        //public bool Type { get; set; }

        public virtual HouseHold HouseHold { get; set; }
        public virtual Category Category { get; set; }
        public virtual Budget Budget { get; set; }

        //public virtual ICollection<Transaction> Transactions { get; set; }
    }
}