﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KingBudget.Models
{
    public class Invite
    {
        public int Id { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public int HouseHoldId { get; set; }
        public bool Join { get; set; } = false;
        //public bool Activation { get; set; } = false;
        //this will send the new user a string to activate their account. 
        public string Secret { get; set; }
    }
}