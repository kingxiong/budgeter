﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KingBudget.Models
{
    public class BankAccount
    {
        public int Id { get; set; }
        public int HouseHoldId { get; set; }
        public string Name { get; set; }
        public double Balance { get; set; }

        public virtual HouseHold Household { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}