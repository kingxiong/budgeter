﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KingBudget.Models.Helper
{
    public class HouseHoldInvite
    {
        private ApplicationDbContext db;
        public HouseHoldInvite(ApplicationDbContext context)
        {
            this.db = context;
        }
        public bool IsHouseholdOnUser(int householdId, string userId)
        {
            var household = db.HouseHolds.Find(householdId);
            var userCheck = household.Users.Any(h => h.Id == userId);
            return (userCheck);
        }
        public ICollection<ApplicationUser> ListAssignedUsers(int householdId)
        {
            HouseHold household = db.HouseHolds.Find(householdId);
            var userList = household.Users.ToList();
            return (userList);
        }
        public bool AddHouseholdToUser(int householdId, string userId)
        {
            HouseHold household = db.HouseHolds.Find(householdId);
            ApplicationUser user = db.Users.Find(userId);
            household.Users.Add(user);
            try
            {
                var userAdded = db.SaveChanges();
                if (userAdded != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public bool RemoveHouseholdFromUser(int householdId, string userId)
        {
            HouseHold household = db.HouseHolds.Find(householdId);
            ApplicationUser user = db.Users.Find(userId);
            var result = household.Users.Remove(user);
            try
            {
                var userRemoved = db.SaveChanges();
                if (userRemoved != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}