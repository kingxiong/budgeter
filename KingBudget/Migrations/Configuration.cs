namespace KingBudget.Migrations
{
    using KingBudget.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KingBudget.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(KingBudget.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var roleManager = new RoleManager<IdentityRole>(
new RoleStore<IdentityRole>(context));

            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
            }

            if (!context.Roles.Any(r => r.Name == "HouseHold Admin"))
            {
                roleManager.Create(new IdentityRole { Name = "HouseHold Admin" });
            }

            if (!context.Roles.Any(r => r.Name == "Member"))
            {
                roleManager.Create(new IdentityRole { Name = "Member" });
            }

            if (!context.Roles.Any(r => r.Name == "Guest"))
            {
                roleManager.Create(new IdentityRole { Name = "Guest" });
            }

            var userManager = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));

            if (!context.Users.Any(u => u.Email == "kxiong388@gmail.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "King",
                    LastName = "Xiong",
                    PhoneNumber = "(828) 461-6238",
                    DisplayName = "Kingaroo",
                    UserName = "kxiong388@gmail.com",
                    Email = "kxiong388@gmail.com",
                }, "Asdf123!");
            }
            if (!context.Users.Any(u => u.Email == "ewatkins@coderfoundry.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Eric",
                    LastName = "Watkins",
                    PhoneNumber = "(###) ###-###",
                    DisplayName = "Radio Voice",
                    UserName = "ewatkins@coderfoundry.com",
                    Email = "ewatkins@coderfoundry.com",
                }, "Asdf123!");
            }
            if (!context.Users.Any(u => u.Email == "barrychau93@gmail.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Barry",
                    LastName = "Chau",
                    PhoneNumber = "(###) ###-###",
                    DisplayName = "Burrito",
                    UserName = "barrychau93@gmail.com",
                    Email = "barrychau93@gmail.com",
                }, "Asdf123!");
            }
            if (!context.Users.Any(u => u.Email == "guest@budgeter.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Guest",
                    LastName = "Guest",
                    PhoneNumber = "(###) ###-###",
                    DisplayName = "Cheetos",
                    UserName = "guest@budgeter.com",
                    Email = "guest@budgeter.com",
                }, "Asdf123!");
            }

            var userId_King = userManager.FindByEmail("kxiong388@gmail.com").Id;
            userManager.AddToRole(userId_King, "Admin");
            var userId_Eric = userManager.FindByEmail("ewatkins@coderfoundry.com").Id;
            userManager.AddToRole(userId_King, "HouseHold Admin");
            var userId_Barry = userManager.FindByEmail("barrychau93@gmail.com").Id;
            userManager.AddToRole(userId_King, "Member");
            var userId_Guest = userManager.FindByEmail("guest@budgeter.com").Id;
            userManager.AddToRole(userId_King, "Guest");
        }
    }
}
