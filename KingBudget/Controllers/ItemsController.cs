﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBudget.Models;
using Microsoft.AspNet.Identity;

namespace KingBudget.Controllers
{
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        // GET: Items
        public ActionResult Index()
        {
            var items = db.Items.Include(i => i.Category).Include(b => b.HouseHold);
            return View(items.ToList());
        }

        [Authorize]
        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransViewModel modelz = new TransViewModel();
            modelz.householdItemz = db.Items.Find(id);
            modelz.householdz = db.HouseHolds.Find(modelz.householdItemz.HouseHoldId);
            foreach (var account in modelz.householdz.BankAccounts)
            {
                foreach (var transaction in account.Transactions.Where(t => t.Type.Name == "Expense"))
                {
                    if (modelz.householdItemz.Category.Name == transaction.Category.Name)
                    {
                        modelz.householdTransaction.Add(transaction);
                    }
                }
            }
            if (modelz.householdItemz == null)
            {
                return HttpNotFound();
            }
            return View(modelz);
        }

        [Authorize]
        // GET: Items/Create
        public ActionResult Create(int id)
        {
            //find household which has the itemId in the parameter ^
            //HouseHold household = db.HouseHolds.Find(db.Items.Find(id).HouseHoldId);
            HouseHold household = db.HouseHolds.Find(id);

            List<Category> householdItemList = household.Items.Select(m => m.Category).ToList();

            var categoryList = new List<Category>();

            categoryList.AddRange(db.Categories);

            foreach(var item in householdItemList)
            {
                categoryList.Remove(item);
            }

            ViewBag.CategoryId = new SelectList(categoryList, "Id", "Name");
            //ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name");
            ViewBag.HouseHoldId = id;
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryId,HouseHoldId,Amount")] Item item, int hhId)
        {
            if (ModelState.IsValid)
            {
                item.HouseHoldId = hhId;
                db.Items.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index","Items");
            }

            ViewBag.HouseHoldId = new SelectList(db.HouseHolds, "Id", "Name", item.HouseHoldId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", item.CategoryId);
            return View(item);
        }

        [Authorize]
        // GET: Items/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            //ViewBag.BudgetId = new SelectList(db.Budgets, "Id", "Name", item.BudgetId);
            ViewBag.CategoryId = item.CategoryId;
            ViewBag.HouseHoldId = item.HouseHoldId;
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CategoryId,HouseHoldId,Amount,IsDeleted")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }
            //ViewBag.BudgetId = new SelectList(db.Budgets, "Id", "Name", item.BudgetId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", item.CategoryId);
            return View(item);
        }

        [Authorize]
        // GET: Items/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
