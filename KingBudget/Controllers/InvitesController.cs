﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBudget.Models;
using KingBudget.Models.Helper;
using System.Net.Mail;
using System.Configuration;
using System.Threading.Tasks;

namespace KingBudget.Controllers
{
    public class InvitesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        // GET: Invites
        public ActionResult Index()
        {
            return View(db.Invites.ToList());
        }

        [Authorize]
        // GET: Invites/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invite invite = db.Invites.Find(id);
            if (invite == null)
            {
                return HttpNotFound();
            }
            return View(invite);
        }

        [Authorize]
        // GET: Invites/Create
        public ActionResult Create(int id)
        {
            ViewBag.HouseHoldId = id;
            return View();
        }

        // POST: Invites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Email,Secret")] Invite invite, int hId)
         {
            if (ModelState.IsValid)
            {

                invite.HouseHoldId = hId;
                invite.Secret = Utilities.GenerateSecretCode();
                var email = new MailMessage(ConfigurationManager.AppSettings["username"], invite.Email)
                {
                    Subject = "Join Household",
                    Body = "You have been invited to join a household in the budget tool. Select -Join- on log in, and input the code: " + invite.Secret + ".\n" +
                    "Or, <a href=\"" + Url.Action("InviteRegister", "Account", new { secret = invite.Secret, HouseholdId = invite.HouseHoldId }, protocol: Request.Url.Scheme) + "\">Click here</a> to join."
                };
                db.Invites.Add(invite);
                db.SaveChanges();
                var svc = new PersonalEmail();
                await svc.SendAsync(email);
                return RedirectToAction("Index","Home");
            }

            return View(invite);

        }

        [Authorize]
        // GET: Invites/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invite invite = db.Invites.Find(id);
            if (invite == null)
            {
                return HttpNotFound();
            }
            return View(invite);
        }

        // POST: Invites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,HouseHoldId,Secret")] Invite invite)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invite).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(invite);
        }

        [Authorize]
        // GET: Invites/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invite invite = db.Invites.Find(id);
            if (invite == null)
            {
                return HttpNotFound();
            }
            return View(invite);
        }

        [Authorize]
        // POST: Invites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invite invite = db.Invites.Find(id);
            db.Invites.Remove(invite);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
