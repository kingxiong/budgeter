﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBudget.Models;
using Microsoft.AspNet.Identity;

namespace KingBudget.Controllers
{
    public class TransactionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        // GET: Transactions
        public ActionResult Index()
        {
            var transactions = db.Transactions.Include(t => t.BankAccount).Include(t => t.Category);
            return View(transactions.ToList());
        }

        [Authorize]
        // GET: Transactions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        [Authorize]
        // GET: Transactions/Create
        public ActionResult Create(int id)
        {

            ViewBag.BankAccountId = id;
            ViewBag.TypeId = new SelectList(db.Types, "Id", "Name");
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name");
            ViewBag.EnteredById = new SelectList(db.Users, "Id", "Name");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BankAccountId,Description,Payee,Date,Amount,TypeId,CategoryId,EnteredById,IsDeleted")] Transaction transaction, int tId)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                transaction.BankAccountId = tId;
                var bankaccount = db.BankAccounts.Find(tId);
                if (bankaccount != null)
                {
                    transaction.BankAccount = bankaccount;
                    transaction.EnteredById = userId;
                    if (transaction.TypeId == 1)
                    {
                        transaction.BankAccount.Balance += transaction.Amount;
                    }
                    else if (transaction.TypeId == 2)
                    {
                        transaction.BankAccount.Balance -= transaction.Amount;
                    }
                    db.Transactions.Add(transaction);
                    transaction.Created = new DateTimeOffset(DateTime.Now);

                    db.SaveChanges();
                    return RedirectToAction("Details", "BankAccounts", new { id = bankaccount.Id});
                }
            }

            
            ViewBag.BankAccountId = tId;
            ViewBag.TypeId = new SelectList(db.Types, "Id", "Name", transaction.TypeId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", transaction.CategoryId);
           
            return View(transaction);
        }

        [Authorize]
        // GET: Transactions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            transaction.BankAccount = db.BankAccounts.Find(transaction.BankAccountId);
            ViewBag.TempBalance = transaction.Amount;
            if (transaction == null)
            {
                return HttpNotFound();
            }

            ViewBag.BankAccountId = new SelectList(db.BankAccounts, "Id", "Name", transaction.BankAccountId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", transaction.CategoryId);
            ViewBag.EnteredById = new SelectList(db.Users, "Id", "Name", transaction.EnteredById);

            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BankAccountId,Description,Payee,Date,Amount,TypeId,CategoryId,EnteredById,IsDeleted")] Transaction transaction, double tempAmount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transaction).State = EntityState.Modified;
                transaction.BankAccount = db.BankAccounts.Find(transaction.BankAccountId);
                if (tempAmount != transaction.Amount)//Chekc to see if user changed the transaction amount
                {
                    if (transaction.TypeId == 1)//id for Income Change to Type
                    {
                        transaction.BankAccount.Balance = transaction.BankAccount.Balance - tempAmount; //reverse the amount
                        transaction.BankAccount.Balance = transaction.BankAccount.Balance + transaction.Amount; //add the edit

                    }
                    else
                    {
                        
                        if (transaction.BankAccount.Balance >= transaction.Amount)
                        {
                            transaction.BankAccount.Balance = transaction.BankAccount.Balance + tempAmount;
                            transaction.BankAccount.Balance = transaction.BankAccount.Balance - transaction.Amount;

                        }
                        else//redirect user to same page if they gover the balannce
                        {
                            return RedirectToAction("Edit", "Transactions", new { id = transaction.Id });
                        }

                    }
                }
                transaction.Created = new DateTimeOffset(DateTime.Now);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.BankAccountId = new SelectList(db.BankAccounts, "Id", "Name", transaction.BankAccountId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", transaction.CategoryId);
            ViewBag.EnteredById = new SelectList(db.Users, "Id", "Name", transaction.EnteredById);
            return View(transaction);
        }

        [Authorize]
        // GET: Transactions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Transaction transaction = db.Transactions.Find(id);
            db.Transactions.Remove(transaction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
